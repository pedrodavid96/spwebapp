'use strict'

const Koa = require('koa')
const path = require('path')
const hbs = require('koa-hbs')
const favicon = require('koa-favicon')
const serve = require('koa-static')
const logger = require('koa-logger')
const bodyParser = require('koa-bodyparser')

const index = require('./routes/index')
const users = require('./routes/users')
const spotify = require('./routes/spotify')

const app = new Koa()

app.use(errorHandler)

app.use(hbs.middleware({
  viewPath: path.join(__dirname, 'views'),
  defaultLayout: 'layout',
  disableCache: app.env === 'development'
}))

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(logger('dev'))
app.use(bodyParser())
app.use(serve('public'))

app.use(index)
app.use(users)
app.use(spotify)

async function errorHandler (ctx, next) {
  try {
    await next()
  } catch (err) {
    // set locals, only providing error in development
    ctx.state.message = err.message
    ctx.state.error = ctx.app.env === 'development' ? err : {}

    // render the error page
    ctx.status = err.status || 500
    ctx.app.emit('error', err, ctx)
    await ctx.render('error')
  }
}

module.exports = app
