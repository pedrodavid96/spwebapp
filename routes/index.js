const router = require('koa-router')()

/* GET home page. */
router.get('/', ctx => ctx.render('index', { title: 'Spotify Web Application' }))

module.exports = router.routes()
