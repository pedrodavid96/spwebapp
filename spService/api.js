'use strict'

const { Artist } = require('./model/Artist')
const { Album } = require('./model/Album')
const { Track } = require('./model/Track')
const Paging = require('./model/Paging')

const baseUrl = 'https://api.spotify.com'

module.exports = {
  search: function (type, q, offset = 0, limit = 20) {
    return {
      url: `${baseUrl}/v1/search?type=${type}&q=${q}&offset=${offset}&limit=${limit}`,
      mapper: res => res
    }
  },
  getArtist: function (id) {
    return {
      url: `${baseUrl}/v1/artists/${id}`,
      mapper: Artist
    }
  },
  getArtistAlbums: function (id, offset = 0, limit = 20) {
    return {
      url: `${baseUrl}/v1/artists/${id}/albums?offset=${offset}&limit=${limit}`,
      mapper: Paging
    }
  },
  getAlbum: function (id) {
    return {
      url: `${baseUrl}/v1/albums/${id}`,
      mapper: Album
    }
  },
  getAlbumTracks: function (id, offset = 0, limit = 20) {
    return {
      url: `${baseUrl}/v1/albums/${id}/tracks?offset=${offset}&limit=${limit}`,
      mapper: Paging
    }
  },
  getTrack: function (id) {
    return {
      url: `${baseUrl}/v1/tracks/${id}`,
      mapper: Track
    }
  }
}
