'use strict'

const fetch = require('isomorphic-fetch')
const fetchAsJson = (url, options) =>
  fetch(url, options).then(resp => resp.json())

const api = require('./api')

module.exports = function SpotifyService (credentials) {
  let getAuthInfoPromise = null;
  (function getAuthInfo () {
    getAuthInfoPromise = fetchAsJson('https://accounts.spotify.com/api/token', {
      method: 'POST',
      headers: {
        'Authorization': `Basic ${Buffer.from(`${credentials.id}:${credentials.secret}`).toString('base64')}`,
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: 'grant_type=client_credentials'
    })
    .then(res => {
      setTimeout(getAuthInfo, (res.expires_in - 5) * 1000)
      return res
    })
  })()
  // Map collection of urls and mappers to api calls with authorization
  return Object.entries(api).reduce((previous, [key, fun]) => {
    const decoratedFunction = (...args) => {
      return getAuthInfoPromise.then(authInfo => {
        const headers = { 'Authorization': `${authInfo.token_type} ${authInfo.access_token}` }
        const {url, mapper} = fun(...args)
        return fetchAsJson(url, {headers}).then(mapper)
      })
    }
    return {...previous, [key]: decoratedFunction}
  }, {})
}
