'use strict'

const { SimpleArtist } = require('./Artist')
const { SimpleAlbum } = require('./Album')

module.exports = {
  SimpleTrack: function (spSimpleTrack) {
    const {
      id,
      name,
      track_number,
      disc_number,
      duration_ms,
      explicit,
      artists,
      preview_url,
      uri
    } = spSimpleTrack
    // array of simple artists
    return {
      id,
      name,
      trackNumber: track_number,
      discNumber: disc_number,
      durationMs: duration_ms,
      explicit,
      artists: artists.map(SimpleArtist),
      previewUrl: preview_url,
      spotifyUri: uri
    }
  },
  Track: function (spTrack) {
    // simple Album
    const {
      id,
      name,
      track_number,
      disc_number,
      duration_ms,
      explicit,
      popularity,
      album,
      artists,
      preview_url,
      uri
    } = spTrack
    // array of simple artists
    return {
      id,
      name,
      trackNumber: track_number,
      discNumber: disc_number,
      durationMs: duration_ms,
      explicit,
      popularity,                         // Only in Track
      album: SimpleAlbum(album),          // Only in Track
      artists: artists.map(SimpleArtist),
      previewUrl: preview_url,
      spotifyUri: uri
    }
  }
}
